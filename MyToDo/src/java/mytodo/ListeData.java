/*
MyToDo
Authors: Svenja Lachmann, Anna Linke, Alisa Reiter
*/

package mytodo;
 
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.ServletContext;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

/*
Klasse für die Verwaltung von Listen

Datenhaltung:
Die Datenhaltung ist hier mit einer XML-Datei realiesiert worden. Die Listen werden in der Datei listen.xml hinterlegt.
Zum Verarbeiten dieser XML-Datei wurde die Bibliothek JDOM verwendet. Über JDOM lassen sich die XML-formatierten Dateien einlesen, manipulieren und dann wieder schreiben.
Die JDOM-Baustruktur im Speicher wird von einem SAX-Parser erzeugt.
JDOM ist freie Software, die auf der Apache-Lizenz beruht. Die Bibliotheken und die dazu gehörige Dokumentation findet man unter  http://www.jdom.org/.
Denkanstöße stammen von folgender Internetadresse: http://gos.dtdns.net/javainsel/javainsel_13_006.htm##mj9eec4682d582ea1a21115eb9c56f72fc.

*/ 

@Named ("listeData")
@SessionScoped

public class ListeData implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    private String titel;
    private static String aktiverUser; //eingeloggter User
    private int id;
    private static String erlaubteUser[];
    private boolean entferne; //wird true, wenn die Berechtigung für Benutzer beim Bearbeiten einer Liste gelöscht werden soll
    private boolean hinzufuegen; //wird true, wenn ein neuer Benutzer zur Liste hinzugefügt werden soll

    private static ArrayList<Liste> listen = new ArrayList<>(); //ArrayList in der alle Listen die aus der XML-Datei ausgelesen werden abgelegt werden.
    private static ArrayList<Liste> listenGefiltert = new ArrayList<>(); //ArrayList in der die Listen abgelegt werden, die nach Benutzer gefiltert wurden.
    private static List<String> bearbeiteUserList = new ArrayList<>(); //ArrayList in der die bereits vorhandenen User zwischengespeichert werden können, damit neue User hinzugefuegt werden können
   
    /**
     * Liefert das XML-File in dem die Listen gespeichert werden
     *
     * @return xmlFile
     */
    public static File getFile() {
        
        // Den absoluten Pfad von "/data/listen.xml" erzeugen
        ServletContext context = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
        String filepath = context.getRealPath("/data/listen.xml");  

        //bestehendes XML-File laden
        File xmlFile = new File(filepath);

        return xmlFile;
    }
    
    /**
     * Speichert das geänderte Document in einem XML-File
     *
     * @param document
     * @param rootElement
     * @param xmlFile
     * @throws IOException
     */
    public void speichereXMLFile(Document document, Element rootElement, File xmlFile) throws IOException {
        
        //Dokument als XML-Datei speichern
        document.setContent(rootElement);
        FileWriter writer = new FileWriter(xmlFile);
        XMLOutputter outputter = new XMLOutputter();
        outputter.setFormat(Format.getPrettyFormat()); //XML Formatierung
        outputter.output(document, writer);
    }
    
    /**
     * Liest die Aufgaben aus XML-File aus
     *
     * @return al
     */
    public static ArrayList<Liste> getListen() {
        
        ArrayList al = new ArrayList(); //liste die am Ende zurückgegeben wird
        ListeData.Liste l; //aufgabe die mit den Daten aus der XML-Datei erstellt wird und an liste al übergeben wird
        List<String> usersXML = new ArrayList<String>(); //List in die die Elemente "User" aus XML-File zwischengespeichert werden
        
        File xmlFile = getFile(); //File erzeugen
        
        SAXBuilder saxBuilder = new SAXBuilder(); //Baut einen XML-Leser auf Basis von SAX auf. Es wird nicht validiert.
        
        try {
            
            Document document = (Document) saxBuilder.build(xmlFile); //Dokument einlesen
            Element rootElement = document.getRootElement(); //Wurzelelement "listen" einlesen
            List listElement = rootElement.getChildren("liste"); //Liste mit allen Kindern des Wurzelelements befüllen (hier alle Kinder "liste" vom Wurzelelement "listen"

            //Schleife über die Liste mit allen Kindern laufen lassen und Elemente auslesen
            for (int i = 0; i < listElement.size(); i++) {

                //Auf einzelnen Knoten zugreifen
                Element node = (Element) listElement.get(i);
                
                 //Liste mit allen Elemente "user" erstellen. Dieser werden dann in Schleife in die ArrayList usersXML zwischengespeichert
                List listUser = node.getChildren("user");

                for (int m = 0; m < listUser.size(); m++) {
                    Element userXML = (Element) listUser.get(m);
                    usersXML.add(userXML.getText());
                }
                //ArrayList usersXML wird in Array umgewandelt. Der der Konstruktor von Liste ein String[] verlangt    
                String[] userXMLArray = usersXML.toArray(new String[usersXML.size()]);
                
                // eine neue Liste mit den Daten aus dem XML-File erstellen (Kontruktor Aufruf)
                l = new ListeData.Liste(
                        node.getAttribute("id").getIntValue(),
                        node.getChildText("titel"),
                        userXMLArray);

                al.add(l);  // die neue Liste zur ArrayList hinzufügen

                usersXML.clear(); //die ArrayList für die Zwischenspeicherung leeren
            }
            
        } catch (IOException | JDOMException io) {
            System.out.println(io.getMessage());
        }

        return al;
    }
    
     /**
     * Filtert die Listen nur nach Erlaubnis.
     * Die gefilterten Listen werden in listenGefiltert abgelegt und im Template angezeigt
     */
    public static void filterListen() {

        listenGefiltert.clear();
        listen = getListen(); //aufgaben aus XML einlesen
        checkErlaubnis();
        for (Liste liste : listen) {
            if (liste.isErlaubnis() == true) {
                listenGefiltert.add(liste);
            }
        }
    }
    
    
    /**
     * Prueft welche Listen der User sehen darf und setzt die Erlaubnis dieser
     * Listen auf true
     */
    public static void checkErlaubnis() {
        listen = getListen();
        for (Liste liste : listen) {
            for(String user : liste.erlaubteUser)
                if (aktiverUser.equals(String.valueOf(user))) {
                    liste.setErlaubnis(true);
                }
            
        }
    }
    
    /**
     * Setzt die Erlaubnis aller Listen auf false
     */
    public static void resetErlaubnis(){
        for(Liste liste: listen){
            liste.setErlaubnis(false);
        }
        
        for(Liste liste1: listenGefiltert){
            liste1.setErlaubnis(false);
        }
    }
    
    /**
     * Erzeugt eine Zufallszahl zwischen 0 und der Anzahl der Listen
     *
     * @param MaxAnzahl
     * @return zufallszahl
     */
    public int erzeugeZufallszahl(int MaxAnzahl) {
        int zufallszahl = (int) ((Math.random() * MaxAnzahl) + 1);
        return zufallszahl;
    }
    
    /**
     * Fuegt eine neue Aufgabe hinzu (ruft Konstruktor auf). Ein neues Element
     * "liste" wird in der XML-Datei hinzugefügt
     *
     * @return alle Aufgaben werden angezeigt
     */
    public String addListe() {

        boolean IDchecked = false;

        SAXBuilder saxBuilder = new SAXBuilder(); //Baut einen XML-Leser auf Basis von SAX auf. Es wird nicht validiert.

        File xmlFile = getFile(); //File erzeugen

        try {

            Document document = (Document) saxBuilder.build(xmlFile); //Dokument einlesen
            Element rootElement = document.getRootElement(); //Wurzelelement "listen" einlesen
            List ListListen = rootElement.getChildren("liste"); //Liste mit allen Kindern des Wurzelelements befüllen (hier alle Kinder "liste" vom Wurzelelement "listen"

            //Anzahl der Listen ergibt sich aus der Größe der Liste von den bereits vorhandenen Listen +1
            int AnzahlListen = ListListen.size() + 1;

            //Eine id für die neue Liste erzeugen. Es wird solange eine Zufallszahl erzeugt, bis eine gefunden wird, die noch keiner Liste zugewiesen ist.
            //Damit ist garantiert, dass die id einer Aufgabe einmalig ist. Somit kann eine Aufgabe eindeutig in der XML-Datei gefunden werden.
            while (IDchecked == false) {
                Vector usedIDs = new Vector(); //In diesem Vector werden die bereits vergebenen Werte zwischengespeichert
                id = erzeugeZufallszahl(AnzahlListen);
                Iterator IteratorListe = ListListen.iterator();

                while (IteratorListe.hasNext()) {
                    Element listeXML = (Element) IteratorListe.next();
                    int listeID = listeXML.getAttribute("id").getIntValue();
                    usedIDs.add(listeID);
                }

                //Abfrage ob die erzeugte Zufallszahl in dem Vector der vergebenen Werte enthalten ist. Wenn nein, wird while-Schleife abgebrochen und die erzeugte Zufallszahl als ID der neuen Aufgabe gesetzt.
                if (!(usedIDs.contains(id))) {
                    IDchecked = true;
                    break;
                }
            }

            //neue Liste mit den eingegebenen Werten anlegen
            Liste liste = new Liste(id, titel, erlaubteUser);

            //Dem Root-Knoten ein neues Elemente "liste" hinzufügen
            Element neueListe = new Element("liste");
            rootElement.addContent(neueListe);

            //Dem neuen Element "liste" die zuvor erzeugte "id" als Attribut hinzufügen
            neueListe.setAttribute("id", String.valueOf(id));

            //Dem neuen Element "liste" neues Element "titel" hinzufügen und mit dem entsprechenden Wert füllen
            Element neueTitel = new Element("titel");
            neueListe.addContent(neueTitel);
            neueListe.getChild("titel").setText(String.valueOf(liste.titel));

            //Dem neuen Element "liste" soviele neue Elemente "user" hinzufügen wie eingegeben wurden
            for (int k = 0; k < (liste.erlaubteUser.length); k++) {
                Element neueUser = new Element("user");
                neueListe.addContent(neueUser);
            }

            //Die neuen Elemente "erlaubteUser" mit dem entsprechenden Wert füllen 
            List neueUserElemente = neueListe.getChildren("user");

            for (int l = 0; l < neueUserElemente.size(); l++) {
                Element neuesUserElement = (Element) neueUserElemente.get(l);
                neuesUserElement.setText(String.valueOf(erlaubteUser[l]));
            }
                      

            //Dokument als File speichern
            speichereXMLFile(document, rootElement, xmlFile);

        } catch (IOException | JDOMException io) {
            System.out.println(io.getMessage());
        }
        
        filterListen();
        mytodo.AufgabeData.filterAlleAufgaben();
        loescheDataWerte();
        return "AlleAufgaben";
    }
       
    /**
     * Loescht eine Liste
     *
     * @param liste Es wird die Liste uebergeben in deren Zeile der Button
     * angeklickt wird. Diese Liste wird geloescht. Der entsprechende Eintrag in listen.xml wird geloescht
     */
    public void deleteListe(Liste liste) {

        SAXBuilder saxBuilder = new SAXBuilder(); //Baut einen XML-Leser auf Basis von SAX auf. Es wird nicht validiert.

        File xmlFile = getFile(); //File erzeugen

        try {

            Document document = (Document) saxBuilder.build(xmlFile); //Dokcument einlesen
            Element rootElement = document.getRootElement(); //Wurzelelement "listen" einlesen

            //Suche nach einem bestimmten Knoten
            List ListListen = rootElement.getChildren("liste");
            Iterator IteratorListe = ListListen.iterator();
            Element ListeSearchFor = null;

            while (IteratorListe.hasNext()) {
                Element listeXML = (Element) IteratorListe.next();
                String listeID = listeXML.getAttributeValue("id");
                if (listeID != null) {
                    if (listeID.equals(String.valueOf(liste.id))) {
                        ListeSearchFor = listeXML;
                        rootElement.removeContent(ListeSearchFor);
                        break;
                    }
                }
            }
            
            //Dokument als File speichern
            speichereXMLFile(document, rootElement, xmlFile);

        } catch (IOException | JDOMException io) {
            System.out.println(io.getMessage());
        }
        
        filterListen();
    }
    
    
    
    /**
     * Gibt die Moeglichkeit zum Bearbeiten von Titel und Benutzer der
     * Liste
     *
     * @param liste Es wird die Liste uebergeben in deren Zeile der Button
     * angeklickt wird 
     * @return  es wird BearbeiteListe.xhtml angezeigt
     */
    public String editListe(Liste liste) {
       
        SAXBuilder saxBuilder = new SAXBuilder(); //Baut einen XML-Leser auf Basis von SAX auf. Es wird nicht validiert.

        File xmlFile = getFile(); //File erzeugen
        
        List<String> usersXML = new ArrayList<String>(); //List in die die Elemente "User" aus XML-File zwischengespeichert werden

        try {

            Document document = (Document) saxBuilder.build(xmlFile); //Document einlesen
            Element rootElement = document.getRootElement(); //Wurzelelement "listen" einlesen

            //Suche nach einem bestimmten Knoten
            List ListListen = rootElement.getChildren("liste");
            Iterator IteratorListe = ListListen.iterator();
            Element ListeSearchFor = null;

            while (IteratorListe.hasNext()) {
                Element listeXML = (Element) IteratorListe.next();
                String listeID = listeXML.getAttributeValue("id");
                if (listeID != null) {
                    if (listeID.equals(String.valueOf(liste.id))) {
                        ListeSearchFor = listeXML;
                        
                        //Liste mit allen Elemente "user" erstellen. Dieser werden dann in Schleife in die ArrayList usersXML zwischengespeichert
                        List listUser = listeXML.getChildren("user");

                        for (int k = 0; k < listUser.size(); k++) {
                            Element userXML = (Element) listUser.get(k);                           
                            usersXML.add(userXML.getText());
                        }
                        
                        //ArrayList listenXML wird in Array umgewandelt. Der der Konstruktor von Aufgabe ein String[] verlangt    
                        String[] usersXMLArray = usersXML.toArray(new String[usersXML.size()]);
                        
                        
                        titel = listeXML.getChildText("titel");
                        bearbeiteUser = usersXMLArray;
                        bearbeiteUserList = usersXML;
                        id = listeXML.getAttribute("id").getIntValue();
                        break;
                    }
                }
            }
            //Dokument als File speichern
            speichereXMLFile(document, rootElement, xmlFile);

        } catch (IOException | JDOMException io) {
            System.out.println(io.getMessage());
        }
        
        return "BearbeiteListe";

    }
    
    /**
     * Speichert die bearbeitete Liste
     * 
     * @return es wird listenverwaltung.xhtml angezeigt
     */
    public String saveAllListe(){
       
        SAXBuilder saxBuilder = new SAXBuilder(); //Baut einen XML-Leser auf Basis von SAX auf. Es wird nicht validiert.

        File xmlFile = getFile(); //File erzeugen

        List<String> usersXML = new ArrayList<String>(); //List in die die Elemente "User" aus XML-File zwischengespeichert
        
        //Wenn die Benutzer nicht geändert wurden, werden die alten Benutzer beibehalten
        if(!(entferne || hinzufuegen)){
            erlaubteUser = bearbeiteUser;
        }
        //Wenn Benutzer gelöscht werden sollen, wird die Funktion zum Entfernen aufgerufen
        if(entferne){
            EntferneUser();
        }
        
        try {

            Document document = (Document) saxBuilder.build(xmlFile); //Document einlesen
            Element rootElement = document.getRootElement(); //Wurzelelement "listen" einlesen

             //Suche nach einem bestimmten Knoten
            List ListListen = rootElement.getChildren("liste");
            Iterator IteratorListe = ListListen.iterator();
            Element ListeSearchFor = null;

            while (IteratorListe.hasNext()) {
                Element listeXML = (Element) IteratorListe.next();
                String listeID = listeXML.getAttributeValue("id");
                if (listeID != null) {
                    if (listeID.equals(String.valueOf(id))) {
                        ListeSearchFor = listeXML;
                        
                         //Alle Elemente "user" löschen, damit sie anschließend neu angelegt werden können
                        listeXML.removeChildren("user");
                        
                        //Dem neuen Element "aufgabe" soviele neue Elemente "liste" hinzufügen wie Listen ausgewählt wurden
                        for (int i = 0; i < (erlaubteUser.length); i++) {
                            Element neueUser = new Element("user");
                            listeXML.addContent(neueUser);
                        }

                        //Die neuen Elemente "liste" mit dem entsprechenden Wert füllen 
                        List neueUserElemente = listeXML.getChildren("user");

                        for (int j = 0; j < neueUserElemente.size(); j++) {
                            Element neuesUserElement = (Element) neueUserElemente.get(j);
                            neuesUserElement.setText(String.valueOf(erlaubteUser[j]));
                        }
                        
                        listeXML.getChild("titel").setText(String.valueOf(titel));
                                               
                        break;
                    }
                }

            }
            //Dokument als File speichern
            speichereXMLFile(document, rootElement, xmlFile);

        } catch (IOException | JDOMException io) {
            System.out.println(io.getMessage());
        }
        
        //Entferne und Hinzufuegen werden am Ende des Bearbeitungsvorgangs zurückgesetzt
        entferne = false;
        hinzufuegen = false;
        
        mytodo.AufgabeData.filterAlleAufgaben();
        filterListen();
        loescheDataWerte();
        
        return "listenverwaltung";
    }
    
    /**
     * Loescht die Werte aus ListeData, die zur Listenerstellung noetig sind,
     * damit das Formular wieder leer erscheint
     */
    public void loescheDataWerte(){
        
        titel = null;
        erlaubteUser = null;
    }
    
    /**
     * Weist den erlaubten User die bearbeiteten User hinzu
     * bearbeiteUser enthält nicht mehr die gelöschten User -> User werden entfernt
     */
    public void EntferneUser(){
        erlaubteUser = bearbeiteUser;
    }
    
        
    /**
     * Definition der Klasse "Liste" ----------------------------------------------------------------------
     */
        public static class Liste{
        int id;
        String titel;      
        private boolean erlaubnis = false;
        private String erlaubteUser[];
        
        
        /**
         * Konstruktor fuer eine Liste
         * @param id id wird gesetzt
         * @param titel Listentitel wird gesetzt         
         * @param erlaubteUser erlaubteUser wird gesetzt
         */
        public Liste (int id, String titel, String[] erlaubteUser){
            this.id = id;
            this.titel = titel;
            this.erlaubteUser = erlaubteUser;
        }
                
        /**
         * Getter & Setter fuer die Klasse Liste
         */
        public String getTitel() {
            return titel;
        }

        public void setTitel(String titel) {
            this.titel = titel;
        }
        
        public boolean isErlaubnis() {
            return erlaubnis;
        }

        public void setErlaubnis(boolean erlaubnis) {
            this.erlaubnis = erlaubnis;
        }
        
        public String[] getErlaubteUser() {
            return erlaubteUser;
        }

        public void setErlaubteUser(String[] erlaubteUser) {
            this.erlaubteUser = erlaubteUser;
        }
        
        
        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }
        
    /**
     * Ende der Definition der Klasse "Liste" ---------------------------------------------------------
     */
    
    /**
     * Getter & Setter der Klasse ListeData
     */
    public String getTitel() {
        return titel;
    }

    public void setTitel(String titel) {
        this.titel = titel;
    }
    
    public static String getAktiverUser() {
        return aktiverUser;
    }

    public static void setAktiverUser(String aktiverUser) {
        ListeData.aktiverUser = aktiverUser;
    }
    
    public static String[] getErlaubteUser() {
        return erlaubteUser;
    }

    public static void setErlaubteUser(String[] erlaubteUser) {
        ListeData.erlaubteUser = erlaubteUser;
    }

    public String[] getBearbeiteUser() {
        return bearbeiteUser;
    }

    public void setBearbeiteUser(String[] bearbeiteUser) {
        this.bearbeiteUser = bearbeiteUser;
    }
    private String[] bearbeiteUser;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public static List<String> getBearbeiteUserList() {
        return bearbeiteUserList;
    }

    public static void setBearbeiteUserList(List<String> bearbeiteUserList) {
        ListeData.bearbeiteUserList = bearbeiteUserList;
    }
    
    public boolean isEntferne() {
        return entferne;
    }

    public void setEntferne(boolean entferne) {
        this.entferne = entferne;
    }

    public boolean isHinzufuegen() {
        return hinzufuegen;
    }

    public void setHinzufuegen(boolean hinzufuegen) {
        this.hinzufuegen = hinzufuegen;
    }
   
    public ArrayList<Liste> getListenGefiltert() {
        return listenGefiltert;
    }

    public void setListenGefiltert(ArrayList<Liste> listenGefiltert) {
        ListeData.listenGefiltert = listenGefiltert;
    }
}
