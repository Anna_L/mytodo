/*
MyToDo
Authors: Svenja Lachmann, Anna Linke, Alisa Reiter
*/

package mytodo;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

/*
Klasse für die Verwaltung von Aufgaben

Datenhaltung:
Die Datenhaltung ist hier mit einer XML-Datei realiesiert worden. Die Listen werden in der Datei aufgaben.xml hinterlegt.
Zum Verarbeiten dieser XML-Datei wurde die Bibliothek JDOM verwendet. Über JDOM lassen sich die XML-formatierten Dateien einlesen, manipulieren und dann wieder schreiben.
Die JDOM-Baustruktur im Speicher wird von einem SAX-Parser erzeugt.
JDOM ist freie Software, die auf der Apache-Lizenz beruht. Die Bibliotheken und die dazu gehörige Dokumentation findet man unter  http://www.jdom.org/.
Denkanstöße stammen von folgender Internetadresse: http://gos.dtdns.net/javainsel/javainsel_13_006.htm##mj9eec4682d582ea1a21115eb9c56f72fc.

*/

@SessionScoped

@Named("aufgabeData")
public class AufgabeData implements Serializable {

    private static final long serialVersionUID = 1L;

    private String[] listen; //Im Array "listen" werden die "ids" der Listen gespeichert, auf denen die Aufgabe sichtbar sein soll. Damit kann einer Aufgabe eindeutig eine Liste zugeordent werden, da die id einmalig ist.
    private String prioritaet; 
    private String titel;
    private String beschreibung;
    private String faellig;
    private boolean ausgeblendet; //wird gesetzt, wenn erledigte Aufgaben ausgeblendet werden sollen
    private String aktiveListe;   
    private boolean canEdit; //wird gesetzt, wenn eine Aufgabe bearbeitet wird
    private static String aktiverUser; //User der eingeloggt ist
    private static String erlaubteUser[];
    private String[] bearbeiteUser;
    private int id;
    private String style; //der style einer Aufgaben entscheidet wie eine Aufgaben (nicht erledigt/erledigt) oder ob eine Aufgabe ausgeblendet wird
    private boolean entferne; //wird true, wenn die Berechtigung für Benutzer beim Bearbeiten einer Aufgabe gelöscht werden soll
    private boolean hinzufuegen; //wird true, wenn ein neuer Benutzer zur Aufgabe hinzugefügt werden soll
    
    private static ArrayList<Aufgabe> aufgaben = new ArrayList<>(); //ArrayList in der alle Aufgaben die aus der XML-Datei ausgelesen werden abgelegt werden
    private static ArrayList<Aufgabe> aufgabenGefiltert = new ArrayList<>(); //ArrayList in der die Aufgaben abgelegt werden, die nach Benutzer und Liste gefiltert wurden. Wird auf Liste.xhtml angezeigt
    private static ArrayList<Aufgabe> aufgabenAlleGefiltert = new ArrayList<>(); //ArrayList in der die Aufgaben abgelegt werden, die nach Benutzer gefilterte wurden. Wird auf AlleAufgaben.xhtml 
    private static List<String> bearbeiteUserList = new ArrayList<>(); //ArrayList in der die bereits vorhandenen User zwischengespeichert werden, damit neue User hinzugefuegt werden können
    
    /**
     * Liefert das XML-File in dem die Aufgaben gespeichert werden
     *
     * @return xmlFile
     */
    public static File getFile() {
        
        // Den absoluten Pfad von "/data/aufgaben.xml" erzeugen
        ServletContext context = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
        String filepath = context.getRealPath("/data/aufgaben.xml");

        //bestehendes XML-File laden
        File xmlFile = new File(filepath);

        return xmlFile;
    }

    /**
     * Speichert das geänderte Document in einer XML-Datei
     *
     * @param document
     * @param rootElement
     * @param xmlFile
     * @throws IOException
     */
    public void speichereXMLFile(Document document, Element rootElement, File xmlFile) throws IOException {
        
        //Dokument als XML-Datei speichern
        document.setContent(rootElement);
        FileWriter writer = new FileWriter(xmlFile);
        XMLOutputter outputter = new XMLOutputter();
        outputter.setFormat(Format.getPrettyFormat()); //XML Formatierung
        outputter.output(document, writer);
    }

    /**
     * Liest die Aufgaben aus XML-File aus
     *
     * @return al
     */
    public static ArrayList<Aufgabe> getAufgaben() {

        ArrayList al = new ArrayList(); //liste die am Ende zurückgegeben wird
        Aufgabe a; //aufgabe die mit den Daten aus dem XML-File erstellt wird und an liste al übergeben wird
        List<String> listenXML = new ArrayList<String>(); //List in die die Elemente "Liste" aus XML-File zwischengespeichert werden
        List<String> usersXML = new ArrayList<String>(); //List in die die Elemente "User" aus XML-File zwischengespeichert werden
                
        File xmlFile = getFile(); //File erzeugen
        
        SAXBuilder saxBuilder = new SAXBuilder(); //Baut einen XML-Leser auf Basis von SAX auf. Es wird nicht validiert.
        
        try {
            
            Document document = (Document) saxBuilder.build(xmlFile); //Dokument erzeugen
            Element rootElement = document.getRootElement(); //Wurzelelement "aufgaben" einlesen
            List listElement = rootElement.getChildren("aufgabe"); //Liste mit allen Kindern des Wurzelelements befüllen (hier alle Kinder "aufgabe" vom Wurzelelement "aufgaben"

            //Schleife über die Liste mit allen Kindern laufen lassen und Elemente auslesen
            for (int i = 0; i < listElement.size(); i++) {

                //Auf einzelnen Knoten zugreifen
                Element node = (Element) listElement.get(i);
     
                //Liste mit allen Elemente "liste" erstellen. Dieser werden dann in Schleife in die ArrayList listenXML zwischengespeichert
                List listListen = node.getChildren("liste");
                
                for (int j = 0; j < listListen.size(); j++) {
                    Element listeXML = (Element) listListen.get(j);
                    listenXML.add(String.valueOf(listeXML.getAttribute("id").getIntValue()));}
                
                //ArrayList listenXML wird in Array umgewandelt, weil der Konstruktor von Aufgabe ein String[] verlangt    
                String[] listenXMLArray = listenXML.toArray(new String[listenXML.size()]);

                //Liste mit allen Elemente "user" erstellen. Dieser werden dann in Schleife in die ArrayList usersXML zwischengespeichert
                List listUser = node.getChildren("user");

                for (int l = 0; l < listUser.size(); l++) {
                    Element userXML = (Element) listUser.get(l);
                    usersXML.add(userXML.getText());}
                
                //ArrayList usersXML wird in Array umgewandelt. Der der Konstruktor von Aufgabe ein String[] verlangt    
                String[] userXMLArray = usersXML.toArray(new String[usersXML.size()]);

                // eine neue Aufgabe mit den Daten aus dem XML-File erstellen (Kontruktor Aufruf)
                a = new Aufgabe(
                        node.getAttribute("id").getIntValue(),
                        listenXMLArray,
                        node.getChildText("prioritaet"),
                        node.getChildText("titel"),
                        node.getChildText("beschreibung"),
                        node.getChildText("faellig"),
                        node.getChildText("style"),
                        node.getChild("edit").getAttribute("canEdit").getBooleanValue(),              
                        userXMLArray);

                al.add(a);  // die neue Aufgabe zur ArrayList al hinzufügen

                listenXML.clear(); //die ArrayList für die Zwischenspeicherung leeren
                usersXML.clear(); //die ArrayList für die Zwischenspeicherung leeren
            }
        } catch (IOException | JDOMException io) {
            System.out.println(io.getMessage());
        }

        return al; //Arraylist al wird zurückgegeben (al=aufgabeData.aufgaben)
    }

    /**
     * Erzeugt eine Zufallszahl zwischen 0 und der Anzahl der Aufgaben.
     * Wird für die Generierung einer zufälligen einmaligen id für jede Aufgabe benötigt
     *
     * @param MaxAnzahl
     * @return zufallszahl
     */
    public int erzeugeZufallszahl(int MaxAnzahl) {
        
        int zufallszahl = (int) ((Math.random() * MaxAnzahl) + 1);
        return zufallszahl;
    }

    /**
     * Prueft welche Aufgaben der User sehen darf und setzt die Erlaubnis dieser
     * Aufgaben auf true
     */
    public static void checkErlaubnis() {
        
        aufgaben = getAufgaben(); //aufgaben aus XML-Datei einlesen
        for (Aufgabe aufgabe : aufgaben) {
            for(String user : aufgabe.erlaubteUser){
                if (aktiverUser.equals(String.valueOf(user))) {
                    aufgabe.setErlaubnis(true);}
            }
        }
    }
    
    /**
     * Setzt die Erlaubnis aller Aufgaben (aufgaben und aufgabenGefiltert) auf
     * false
     */
    public static void resetErlaubnis() {
        
        for (Aufgabe aufgabe : aufgaben){
            aufgabe.setErlaubnis(false);
        }
        
        for (Aufgabe aufgabe1 : aufgabenGefiltert){
            aufgabe1.setErlaubnis(false);
        }
    }

    /**
     * Fuegt eine neue Aufgabe hinzu (ruft Konstruktor auf). Ein neues Element
     * "aufgabe" wird in der XML-Datei hinzugefügt
     *
     * @return alle Aufgaben werden angezeigt
     */
    public String addAufgabe() {

        boolean IDchecked = false; //Wird auf true gesetzt sobald eine id für die Aufgaben gefunden wurde

        SAXBuilder saxBuilder = new SAXBuilder(); //Baut einen XML-Leser auf Basis von SAX auf. Es wird nicht validiert.

        File xmlFile = getFile(); //File erzeugen

        try {

            Document document = (Document) saxBuilder.build(xmlFile); //Document erzeugen
            Element rootElement = document.getRootElement(); //Wurzelelement "aufgaben" einlesen
            List ListAufgabe = rootElement.getChildren("aufgabe"); //Liste mit allen Kindern des Wurzelelements befüllen (hier alle Kinder "aufgabe" vom Wurzelelement "aufgaben"

            //Anzahl der Aufgaben ergibt sich aus der Größe der Liste von den bereits vorhandenen Aufgaben +1
            int AnzahlAufgaben = ListAufgabe.size() + 1;

            //Eine id für die neue Aufgabe erzeugen. Es wird solange eine Zufallszahl erzeugt, bis eine gefunden wird, die noch keiner Aufgabe zugewiesen ist.
            //Damit ist garantiert, dass die id einer Aufgabe einmalig ist. Somit kann eine Aufgabe eindeutig in der XML-Datei gefunden werden.
            while (IDchecked == false) {
                Vector usedIDs = new Vector(); //In diesem Vector werden die bereits vergebenen Werte zwischengespeichert
                id = erzeugeZufallszahl(AnzahlAufgaben); //Zufallszahl erzeugen
                Iterator IteratorAufgabe = ListAufgabe.iterator();
                
                //Den Vector usedIDs mit den bereits vergebenen Werten befüllen
                while (IteratorAufgabe.hasNext()) {
                    Element aufgabeXML = (Element) IteratorAufgabe.next();
                    int aufgabeID = aufgabeXML.getAttribute("id").getIntValue();
                    usedIDs.add(aufgabeID);
                }

                //Abfrage ob die erzeugte Zufallszahl in dem Vector der vergebenen Werte enthalten ist. Wenn nein, wird while-Schleife abgebrochen und die erzeugte Zufallszahl als ID der neuen Aufgabe gesetzt.
                if (!(usedIDs.contains(id))) {
                    IDchecked = true;
                    break;
                }
            }

            //neue Aufgabe mit den eingegebenen Werten (Werte der Klasse AufgabeData) anlegen
            Aufgabe aufgabe = new Aufgabe(id, listen, prioritaet, titel, beschreibung, faellig, style, false, erlaubteUser);

            //Dem Root-Knoten ein neues Elemente "aufgabe" hinzufügen
            Element neueAufgabe = new Element("aufgabe");
            rootElement.addContent(neueAufgabe);

            //Dem neuen Element "aufgabe" die zuvor erzeugte "id" als Attribut hinzufügen
            neueAufgabe.setAttribute("id", String.valueOf(id));

            //Dem neuen Element "aufgabe" soviele neue Elemente "liste" hinzufügen wie Listen ausgewählt wurden
            for (int i = 0; i < (aufgabe.listen.length); i++) {
                Element neueListe = new Element("liste");
                neueAufgabe.addContent(neueListe);
            }

            //Die neuen Elemente "liste" mit dem entsprechenden Wert (id der ausgewählten Liste) füllen 
            List neueListenElemente = neueAufgabe.getChildren("liste");

            for (int j = 0; j < neueListenElemente.size(); j++) {
                Element neuesListenElement = (Element) neueListenElemente.get(j);
                //neuesListenElement.setText(String.valueOf(listen[j].titel));
                neuesListenElement.setAttribute("id", String.valueOf(listen[j]));
            }

            //Dem neuen Element "aufgabe" neues Element "prioritaet" hinzufügen und mit dem entsprechenden Wert füllen
            Element neuePrioritaet = new Element("prioritaet");
            neueAufgabe.addContent(neuePrioritaet);
            neueAufgabe.getChild("prioritaet").setText(String.valueOf(aufgabe.prioritaet));

            //Dem neuen Element "aufgabe" neues Element "titel" hinzufügen und mit dem entsprechenden Wert füllen
            Element neueTitel = new Element("titel");
            neueAufgabe.addContent(neueTitel);
            neueAufgabe.getChild("titel").setText(String.valueOf(aufgabe.titel));

            //Dem neuen Element "aufgabe" neues Element "beschreibung" hinzufügen und mit dem entsprechenden Wert füllen
            Element neueBeschreibung = new Element("beschreibung");
            neueAufgabe.addContent(neueBeschreibung);
            neueAufgabe.getChild("beschreibung").setText(String.valueOf(aufgabe.beschreibung));

            //Dem neuen Element "aufgabe" neues Element "faellig" hinzufügen und mit dem entsprechenden Wert füllen
            Element neueFaellig = new Element("faellig");
            neueAufgabe.addContent(neueFaellig);
            neueAufgabe.getChild("faellig").setText(String.valueOf(aufgabe.faellig));

            //Dem neuen Element "aufgabe" neues Element "style" hinzufügen und "null" zuweisen. Aufgabe wird somit zu Beginn als nicht erledigt angezeigt
            Element neueStyle = new Element("style");
            neueAufgabe.addContent(neueStyle);
            neueAufgabe.getChild("style").setText("null");

            //Dem neuen Element "aufgabe" neues Element "edit" hinzufügen und dem Attribut "canEdit" den Wert "false" zuweisen
            Element neueEdit = new Element("edit");
            neueAufgabe.addContent(neueEdit);
            neueAufgabe.getChild("edit").setAttribute("canEdit", "false");
            
            //Dem neuen Element "aufgabe" soviele neue Elemente "user" hinzufügen wie eingegeben wurden
            for (int k = 0; k < (aufgabe.erlaubteUser.length); k++) {
                Element neueUser = new Element("user");
                neueAufgabe.addContent(neueUser);
            }

            //Die neuen Elemente "erlaubteUser" mit dem entsprechenden Wert füllen 
            List neueUserElemente = neueAufgabe.getChildren("user");

            for (int l = 0; l < neueUserElemente.size(); l++) {
                Element neuesUserElement = (Element) neueUserElemente.get(l);
                neuesUserElement.setText(String.valueOf(erlaubteUser[l]));
            }

            //Dokument als XML-Datei speichern
            speichereXMLFile(document, rootElement, xmlFile);
            
        } catch (IOException | JDOMException io) {
            System.out.println(io.getMessage());
        }

        filterAlleAufgaben(); 
        loescheDataWerte();
        
        return "AlleAufgaben";
    }
    
    /**
     * Loescht die Werte aus AufgabeData, die zur Aufgabenerstellung noetig sind,
     * damit das Formular wieder leer erscheint
     */
    public void loescheDataWerte(){
        listen = null;
        prioritaet = null;
        titel = null;
        beschreibung = null;
        faellig = null;
        erlaubteUser = null;
    }

    /**
     * Loescht eine Aufgabe
     *
     * @param aufgabe Es wird die Aufgabe uebergeben in deren Zeile der Button
     * angeklickt wird. Diese Aufgabe wird geloescht. Der entsprechende Eintrag in aufgaben.xml wird geloescht
     */
    public void deleteAufgabe(Aufgabe aufgabe) {

        SAXBuilder saxBuilder = new SAXBuilder(); //Baut einen XML-Leser auf Basis von SAX auf. Es wird nicht validiert.

        File xmlFile = getFile(); //File erzeugen

        try {

            Document document = (Document) saxBuilder.build(xmlFile); //Dokument erzeugen
            Element rootElement = document.getRootElement(); //Wurzelelement "aufgaben" einlesen

            //Suche nach einem bestimmten Knoten. Schleife über alle Aufgaben laufen lassen, bis die Aufgabe mit der id, die übergeben wurde, gefunden wird
            List ListAufgabe = rootElement.getChildren("aufgabe"); //Liste mit allen Kindern des Wurzelelements befüllen (hier alle Kinder "aufgabe" vom Wurzelelement "aufgaben"
            Iterator IteratorAufgabe = ListAufgabe.iterator();
            Element AufgabeSearchFor = null;
            
            while (IteratorAufgabe.hasNext()) {
                Element aufgabeXML = (Element) IteratorAufgabe.next();
                String aufgabeID = aufgabeXML.getAttributeValue("id");
                if (aufgabeID != null) {
                    if (aufgabeID.equals(String.valueOf(aufgabe.id))) {
                        AufgabeSearchFor = aufgabeXML;
                        rootElement.removeContent(AufgabeSearchFor);
                        break;
                    }
                }
            }
            
            //Dokument als XML-Datei speichern
            speichereXMLFile(document, rootElement, xmlFile);

        } catch (IOException | JDOMException io) {
            System.out.println(io.getMessage());
        }

        filterAlleAufgaben();
        filterAufgabe(aktiveListe);

    }

    /**
     * Die Aufgabe wird von der Liste entfernt, ist danach jedoch noch unter
     * alle Aufgaben zu finden. Der Eintrag für die Liste mit der entsprechende id wird bei der Aufgabe in aufgaben.xml gelöscht
     *
     * @param aufgabe Es wird die Aufgabe uebergeben in deren Zeile der Button
     * angeklickt wird. 
     */
    public void deleteFromList(Aufgabe aufgabe) {

        SAXBuilder saxBuilder = new SAXBuilder(); //Baut einen XML-Leser auf Basis von SAX auf. Es wird nicht validiert.

        File xmlFile = getFile(); //File erzeugen

        try {

            Document document = (Document) saxBuilder.build(xmlFile); //Dokument erzeugen
            Element rootElement = document.getRootElement(); //Wurzelelement "aufgaben" einlesen

            //Suche nach einem bestimmten Knoten. Schleife über alle Aufgaben laufen lassen, bis die Aufgabe mit der id, die übergeben wurde, gefunden wird
            List ListAufgabe = rootElement.getChildren("aufgabe"); //Liste mit allen Kindern des Wurzelelements befüllen (hier alle Kinder "aufgabe" vom Wurzelelement "aufgaben"
            Iterator IteratorAufgabe = ListAufgabe.iterator();
            Element AufgabeSearchFor = null;

            while (IteratorAufgabe.hasNext()) {
                Element aufgabeXML = (Element) IteratorAufgabe.next();
                String aufgabeID = aufgabeXML.getAttributeValue("id");
                if (aufgabeID != null) {
                    if (aufgabeID.equals(String.valueOf(aufgabe.id))) {
                        AufgabeSearchFor = aufgabeXML;

                        List ListenElemente = AufgabeSearchFor.getChildren("liste");

                        for (int j = 0; j < ListenElemente.size(); j++) {
                            Element ListenElement = (Element) ListenElemente.get(j);
                            String ListenElementText = ListenElement.getAttributeValue("id");
                            if (ListenElementText.equals(String.valueOf(aktiveListe))) {
                                AufgabeSearchFor.removeContent(ListenElement);
                            }
                        }
                        break;
                    }
                }
            }
            //Dokument als File speichern
            speichereXMLFile(document, rootElement, xmlFile);

        } catch (IOException | JDOMException io) {
            System.out.println(io.getMessage());
        }

        aufgabenGefiltert.remove(aufgabe);
    }

    /**
     * Wird beim Loeschen einer Liste aufgerufen. Alle Aufgaben die der Liste
     * zugeordnet waren, werden von der Liste geloescht. Der Eintrag für die Liste mit der entsprechende id wird bei allen Aufgaben in aufgaben.xml gelöscht
     * Die Aufgaben sind noch unter "AlleAufgaben" zu finden. 
     *
     * @param liste Bevor die Liste endgueltig geloescht wird, werden die
     * zugeordneten Aufgaben entfernt
     */
    public void deleteAllFromList(ListeData.Liste liste) {

        SAXBuilder saxBuilder = new SAXBuilder(); //Baut einen XML-Leser auf Basis von SAX auf. Es wird nicht validiert.

        File xmlFile = getFile(); //File erzeugen

        try {

            Document document = (Document) saxBuilder.build(xmlFile); //Dokument erzeugen
            Element rootElement = document.getRootElement(); //Wurzelelement "aufgaben" einlesen

            //Suche nach einem bestimmten Knoten. Schleife über alle Aufgaben laufen lassen, bis die Aufgabe mit der id, die übergeben wurde, gefunden wird
            List ListAufgabe = rootElement.getChildren("aufgabe"); //Liste mit allen Kindern des Wurzelelements befüllen (hier alle Kinder "aufgabe" vom Wurzelelement "aufgaben"
            Iterator IteratorAufgabe = ListAufgabe.iterator();

            while (IteratorAufgabe.hasNext()) {
                Element aufgabeXML = (Element) IteratorAufgabe.next();

                List ListenElemente = aufgabeXML.getChildren("liste");

                for (int j = 0; j < ListenElemente.size(); j++) {
                    Element ListenElement = (Element) ListenElemente.get(j);
                    String ListenElementText = ListenElement.getAttributeValue("id");
                    if (ListenElementText.equals(String.valueOf(aktiveListe))) {
                        aufgabeXML.removeContent(ListenElement);
                    }
                }

            }

            //Dokument als XML-Datei speichern
            speichereXMLFile(document, rootElement, xmlFile);

        } catch (IOException | JDOMException io) {
            System.out.println(io.getMessage());
        }

    }

    /**
     * Gibt die Moeglichkeit zum Bearbeiten von Titel und Beschreibung der
     * Aufgabe
     *
     * @param aufgabe Es wird die Aufgabe uebergeben in deren Zeile der Button
     * angeklickt wird. aufgabeData.canEdit und das Attribut "canEdit" der entsprechende Aufgabe in aufgaben.xml wird true gesetzt 
     */
    public void editAufgabe(Aufgabe aufgabe) {
        
        SAXBuilder saxBuilder = new SAXBuilder(); //Baut einen XML-Leser auf Basis von SAX auf. Es wird nicht validiert.

        File xmlFile = getFile(); //File erzeugen

        try {

            Document document = (Document) saxBuilder.build(xmlFile); //Dokument erzeugen
            Element rootElement = document.getRootElement(); //Wurzelelement "aufgaben" einlesen

            //Suche nach einem bestimmten Knoten. Schleife über alle Aufgaben laufen lassen, bis die Aufgabe mit der id, die übergeben wurde, gefunden wird
            List ListAufgabe = rootElement.getChildren("aufgabe"); //Liste mit allen Kindern des Wurzelelements befüllen (hier alle Kinder "aufgabe" vom Wurzelelement "aufgaben"
            Iterator IteratorAufgabe = ListAufgabe.iterator();
            Element AufgabeSearchFor = null;

            while (IteratorAufgabe.hasNext()) {
                Element aufgabeXML = (Element) IteratorAufgabe.next();
                String aufgabeID = aufgabeXML.getAttributeValue("id");
                if (aufgabeID != null) {
                    if (aufgabeID.equals(String.valueOf(aufgabe.id))) {
                        AufgabeSearchFor = aufgabeXML;
                        aufgabeXML.getChild("edit").getAttribute("canEdit").setValue("true");
                        titel = aufgabeXML.getChildText("titel");
                        beschreibung = aufgabeXML.getChildText("beschreibung");
                        break;
                    }
                }
            }
            //Dokument als XML-Datei speichern
            speichereXMLFile(document, rootElement, xmlFile);

        } catch (IOException | JDOMException io) {
            System.out.println(io.getMessage());
        }
        
        canEdit = true;

        filterAufgabe(aktiveListe);
        filterAlleAufgaben();
    }

    /**
     * Gibt die Moeglichkeit zum vollstaendigen Bearbeiten einer Aufgabe
     *
     * @param aufgabe Es wird die Aufgabe uebergeben in deren Zeile der Button
     * angeklickt wird. 
     */
    public String editAllAufgabe(Aufgabe aufgabe) {
       
        SAXBuilder saxBuilder = new SAXBuilder(); //Baut einen XML-Leser auf Basis von SAX auf. Es wird nicht validiert.

        File xmlFile = getFile(); //File erzeugen

        List<String> listenXML = new ArrayList<String>(); //List in die die Elemente "Liste" aus XML-File zwischengespeichert werden
        List<String> usersXML = new ArrayList<String>(); //List in die die Elemente "User" aus XML-File zwischengespeichert werden
        
        try {

            Document document = (Document) saxBuilder.build(xmlFile); //Dokument erzeugen
            Element rootElement = document.getRootElement(); //Wurzelelement "aufgaben" einlesen

            //Suche nach einem bestimmten Knoten. Schleife über alle Aufgaben laufen lassen, bis die Aufgabe mit der id, die übergeben wurde, gefunden wird
            List ListAufgabe = rootElement.getChildren("aufgabe"); //Liste mit allen Kindern des Wurzelelements befüllen (hier alle Kinder "aufgabe" vom Wurzelelement "aufgaben"
            Iterator IteratorAufgabe = ListAufgabe.iterator();
            Element AufgabeSearchFor = null;

            while (IteratorAufgabe.hasNext()) {
                Element aufgabeXML = (Element) IteratorAufgabe.next();
                String aufgabeID = aufgabeXML.getAttributeValue("id");
                if (aufgabeID != null) {
                    if (aufgabeID.equals(String.valueOf(aufgabe.id))) {
                        AufgabeSearchFor = aufgabeXML;
                        
                        //Liste mit allen Elemente "liste" erstellen. Dieser werden dann in Schleife in die ArrayList listenXML zwischengespeichert
                        List listListen = aufgabeXML.getChildren("liste");

                        for (int j = 0; j < listListen.size(); j++) {
                            Element listeXML = (Element) listListen.get(j);
                            //listenXML[j]=listeXML.getText();
                            listenXML.add(listeXML.getAttributeValue("id"));
                        }
                        //ArrayList listenXML wird in Array umgewandelt. Der der Konstruktor von Aufgabe ein String[] verlangt    
                        String[] listenXMLArray = listenXML.toArray(new String[listenXML.size()]);
                        
                        //Liste mit allen Elemente "user" erstellen. Dieser werden dann in Schleife in die ArrayList usersXML zwischengespeichert
                        List listUser = aufgabeXML.getChildren("user");

                        for (int k = 0; k < listUser.size(); k++) {
                            Element userXML = (Element) listUser.get(k);
                            //listenXML[j]=listeXML.getText();
                            usersXML.add(userXML.getText());
                        }
                        //ArrayList listenXML wird in Array umgewandelt. Der der Konstruktor von Aufgabe ein String[] verlangt    
                        String[] usersXMLArray = usersXML.toArray(new String[usersXML.size()]);

                        listen = listenXMLArray;
                        prioritaet = aufgabeXML.getChildText("prioritaet");
                        titel = aufgabeXML.getChildText("titel");
                        beschreibung = aufgabeXML.getChildText("beschreibung");
                        faellig = aufgabeXML.getChildText("faellig");
                        bearbeiteUser = usersXMLArray;
                        bearbeiteUserList = usersXML;
                        id = aufgabeXML.getAttribute("id").getIntValue();
                        break;
                    }
                }
            }
            //Dokument als File speichern
            speichereXMLFile(document, rootElement, xmlFile);

        } catch (IOException | JDOMException io) {
            System.out.println(io.getMessage());
        }

        return "BearbeiteAufgabe";
    }

    /**
     * Speichert die Aenderungen der einzelnen Aufgabe beim Schnellbearbeiten
     *
     * @param aufgabe Es wird die Aufgabe uebergeben in deren Zeile der Button
     * angeklickt wurde. aufgabeData.canEdit und das Attribut der Aufgabe "canEdit" in aufgaben.xml wird false gesetzt
     */
    public void saveAufgabe(Aufgabe aufgabe) {

        SAXBuilder saxBuilder = new SAXBuilder(); //Baut einen XML-Leser auf Basis von SAX auf. Es wird nicht validiert.

        File xmlFile = getFile(); //File erzeugen

        try {

            Document document = (Document) saxBuilder.build(xmlFile); //Dokument erzeugen
            Element rootElement = document.getRootElement(); //Wurzelelement "aufgaben" einlesen

            //Suche nach einem bestimmten Knoten. Schleife über alle Aufgaben laufen lassen, bis die Aufgabe mit der id, die übergeben wurde, gefunden wird
            List ListAufgabe = rootElement.getChildren("aufgabe"); //Liste mit allen Kindern des Wurzelelements befüllen (hier alle Kinder "aufgabe" vom Wurzelelement "aufgaben"
            Iterator IteratorAufgabe = ListAufgabe.iterator();
            Element AufgabeSearchFor = null;

            while (IteratorAufgabe.hasNext()) {
                Element aufgabeXML = (Element) IteratorAufgabe.next();
                String aufgabeID = aufgabeXML.getAttributeValue("id");
                if (aufgabeID != null) {
                    if (aufgabeID.equals(String.valueOf(aufgabe.id))) {
                        AufgabeSearchFor = aufgabeXML;
                        aufgabeXML.getChild("titel").setText(String.valueOf(titel));
                        aufgabeXML.getChild("beschreibung").setText(String.valueOf(beschreibung));
                        aufgabeXML.getChild("edit").getAttribute("canEdit").setValue("false");
                        break;
                    }
                }

            }
            //Dokument als File speichern
            speichereXMLFile(document, rootElement, xmlFile);

        } catch (IOException | JDOMException io) {
            System.out.println(io.getMessage());
        }

        setCanEdit(false);

        filterAufgabe(aktiveListe);
        filterAlleAufgaben();

    }

    /**
     * Speichert die vollstaendig bearbeitete Aufgabe.
     * 
     */
    public String saveAllAufgabe() {
       
        SAXBuilder saxBuilder = new SAXBuilder(); //Baut einen XML-Leser auf Basis von SAX auf. Es wird nicht validiert.

        File xmlFile = getFile(); //File erzeugen

        List<String> listenXML = new ArrayList<String>(); //List in die die Elemente "Liste" aus XML-File zwischengespeichert
        List<String> usersXML = new ArrayList<String>(); //List in die die Elemente "User" aus XML-File zwischengespeichert
        
        //Wenn die Benutzer nicht geändert wurden, werden die alten Benutzer beibehalten
        if(!(entferne || hinzufuegen)){
            erlaubteUser = bearbeiteUser;
        }
        //Wenn Benutzer gelöscht werden sollen, wird die Funktion zum Entfernen aufgerufen
        if(entferne){
            EntferneUser();
        }
        
        try {

            Document document = (Document) saxBuilder.build(xmlFile); //Dokument erzeugen
            Element rootElement = document.getRootElement(); //Wurzelelement "aufgaben" einlesen

            //Suche nach einem bestimmten Knoten. Schleife über alle Aufgaben laufen lassen, bis die Aufgabe mit der id, die übergeben wurde, gefunden wird
            List ListAufgabe = rootElement.getChildren("aufgabe"); //Liste mit allen Kindern des Wurzelelements befüllen (hier alle Kinder "aufgabe" vom Wurzelelement "aufgaben"
            Iterator IteratorAufgabe = ListAufgabe.iterator();
            Element AufgabeSearchFor = null;

            while (IteratorAufgabe.hasNext()) {
                Element aufgabeXML = (Element) IteratorAufgabe.next();
                String aufgabeID = aufgabeXML.getAttributeValue("id");
                if (aufgabeID != null) {
                    if (aufgabeID.equals(String.valueOf(id))) {
                        AufgabeSearchFor = aufgabeXML;
                    
                        //Alle Elemente "liste" löschen, damit sie anschließend neu angelegt werden können
                        aufgabeXML.removeChildren("liste");
                        
                        //Dem neuen Element "aufgabe" soviele neue Elemente "liste" hinzufügen wie Listen ausgewählt wurden
                        for (int i = 0; i < (listen.length); i++) {
                            Element neueListe = new Element("liste");
                            aufgabeXML.addContent(neueListe);
                        }

                        //Die neuen Elemente "liste" mit dem entsprechenden Wert füllen 
                        List neuenListenElemente = aufgabeXML.getChildren("liste");

                        for (int j = 0; j < neuenListenElemente.size(); j++) {
                            Element neuesListenElement = (Element) neuenListenElemente.get(j);
                            neuesListenElement.setAttribute("id", String.valueOf(listen[j]));
                        }
                        
                        //Alle Elemente "user" löschen, damit sie anschließend neu angelegt werden können
                        aufgabeXML.removeChildren("user");
                        
                        //Dem neuen Element "aufgabe" soviele neue Elemente "user" hinzufügen wie User ausgewählt wurden
                        for (int i = 0; i < (erlaubteUser.length); i++) {
                            Element neueUser = new Element("user");
                            aufgabeXML.addContent(neueUser);
                        }

                        //Die neuen Elemente "user" mit dem entsprechenden Wert aus aufgabeData füllen 
                        List neueUserElemente = aufgabeXML.getChildren("user");

                        for (int j = 0; j < neueUserElemente.size(); j++) {
                            Element neuesUserElement = (Element) neueUserElemente.get(j);
                            neuesUserElement.setText(String.valueOf(erlaubteUser[j]));
                        }
                        
                        //Die Elemente "prioritaet", "titel", "beschreibung", "faellig" mit den eingegebnen Werten aus aufgabeData füllen
                        aufgabeXML.getChild("prioritaet").setText(String.valueOf(prioritaet));
                        aufgabeXML.getChild("titel").setText(String.valueOf(titel));
                        aufgabeXML.getChild("beschreibung").setText(String.valueOf(beschreibung));
                        aufgabeXML.getChild("faellig").setText(String.valueOf(faellig));
                        
                        break;
                    }
                }

            }
            //Dokument als File speichern
            speichereXMLFile(document, rootElement, xmlFile);

        } catch (IOException | JDOMException io) {
            System.out.println(io.getMessage());
        }
        
        //Entferne und Hinzufuegen werden am Ende des Bearbeitungsvorgangs zurückgesetzt
        entferne = false;
        hinzufuegen = false;
        
        filterAlleAufgaben();
        filterAufgabe(aktiveListe);
        loescheDataWerte();
        
        return "AlleAufgaben";
    }
    
    /**
     * Weist den erlaubten User die bearbeiteten User hinzu
     * bearbeiteUser enthält nicht mehr die gelöschten User -> User werden entfernt
     */
    public void EntferneUser(){
        erlaubteUser = bearbeiteUser;
    }

    /**
     * Aendert den style einer Aufgabe zu erledigt
     *
     * @param aufgabe Es wird die Aufgabe uebergeben in deren Zeile der Button
     * angeklickt wird aufgabe.style wird "erledigt" gesetzt Wenn ausgeblendet =
     * true wird aufgabe.style auf "ausgeblendet" gesetzt
     */
    public void erledigeAufgabe(Aufgabe aufgabe) {

        SAXBuilder saxBuilder = new SAXBuilder(); //Baut einen XML-Leser auf Basis von SAX auf. Es wird nicht validiert.

        File xmlFile = getFile(); //File erzeugen

        try {

            Document document = (Document) saxBuilder.build(xmlFile); //Dokument erzeugen
            Element rootElement = document.getRootElement(); //Wurzelelement "aufgaben" einlesen

            //Suche nach einem bestimmten Knoten. Schleife über alle Aufgaben laufen lassen, bis die Aufgabe mit der id, die übergeben wurde, gefunden wird
            List ListAufgabe = rootElement.getChildren("aufgabe"); //Liste mit allen Kindern des Wurzelelements befüllen (hier alle Kinder "aufgabe" vom Wurzelelement "aufgaben"
            Iterator IteratorAufgabe = ListAufgabe.iterator();
            Element AufgabeSearchFor = null;

            while (IteratorAufgabe.hasNext()) {
                Element aufgabeXML = (Element) IteratorAufgabe.next();
                String aufgabeID = aufgabeXML.getAttributeValue("id");
                if (aufgabeID != null) {
                    if (aufgabeID.equals(String.valueOf(aufgabe.id))) {
                        AufgabeSearchFor = aufgabeXML;
                        AufgabeSearchFor.getChild("style").setText("erledigt");
                        if (ausgeblendet) {
                            AufgabeSearchFor.getChild("style").setText("ausgeblendet");
                        }
                        break;
                    }
                }
            }
            //Dokument als File speichern
            speichereXMLFile(document, rootElement, xmlFile);

        } catch (IOException | JDOMException io) {
            System.out.println(io.getMessage());
        }

        filterAlleAufgaben();
        filterAufgabe(aktiveListe);
       
    }

    /**
     * Aendert die styleClass einer erledigten Aufgabe zu ausgeblendet
     * aufgabe.style = ausgeblendet fuer alle erledigten Aufgaben
     */
    public void erledigteAusblenden() {

        SAXBuilder saxBuilder = new SAXBuilder(); //Baut einen XML-Leser auf Basis von SAX auf. Es wird nicht validiert.

        File xmlFile = getFile(); //File erzeugen

        try {

            Document document = (Document) saxBuilder.build(xmlFile); //Dokument erzeugen
            Element rootElement = document.getRootElement(); //Wurzelelement "aufgaben" einlesen

            //Suche nach einem bestimmten Knoten. Schleife über alle Aufgaben laufen lassen, bis die Aufgabe mit der id, die übergeben wurde, gefunden wird
            List ListAufgabe = rootElement.getChildren("aufgabe"); //Liste mit allen Kindern des Wurzelelements befüllen (hier alle Kinder "aufgabe" vom Wurzelelement "aufgaben"
            Iterator IteratorAufgabe = ListAufgabe.iterator();
            Element AufgabeSearchFor = null;

            while (IteratorAufgabe.hasNext()) {
                Element aufgabeXML = (Element) IteratorAufgabe.next();
                String aufgabeStyle = aufgabeXML.getChildText("style");
                if (aufgabeStyle != null) {
                    if (aufgabeStyle.equals("erledigt")) {
                        AufgabeSearchFor = aufgabeXML;
                        AufgabeSearchFor.getChild("style").setText("ausgeblendet");

                    }
                }
            }
            //Dokument als File speichern
            speichereXMLFile(document, rootElement, xmlFile);

        } catch (IOException | JDOMException io) {
            System.out.println(io.getMessage());
        }

        ausgeblendet = true;
        
        filterAlleAufgaben();
        filterAufgabe(aktiveListe);
    }

    /**
     * Aendert die styleClass einer erledigten Aufgabe wieder zu erledigt
     * aufgabe.style = erledigt fuer alle ausgeblendeten Aufgaben
     */
    public void erledigteEinblenden() {

        SAXBuilder saxBuilder = new SAXBuilder(); //Baut einen XML-Leser auf Basis von SAX auf. Es wird nicht validiert.

        File xmlFile = getFile(); //File erzeugen

        try {

            Document document = (Document) saxBuilder.build(xmlFile); //Dokument erzeugen
            Element rootElement = document.getRootElement(); //Wurzelelement "aufgaben" einlesen

            //Suche nach einem bestimmten Knoten. Schleife über alle Aufgaben laufen lassen, bis die Aufgabe mit der id, die übergeben wurde, gefunden wird
            List ListAufgabe = rootElement.getChildren("aufgabe"); //Liste mit allen Kindern des Wurzelelements befüllen (hier alle Kinder "aufgabe" vom Wurzelelement "aufgaben"
            Iterator IteratorAufgabe = ListAufgabe.iterator();
            Element AufgabeSearchFor = null;

            while (IteratorAufgabe.hasNext()) {
                Element aufgabeXML = (Element) IteratorAufgabe.next();
                String aufgabeStyle = aufgabeXML.getChildText("style");
                if (aufgabeStyle != null) {
                    if (aufgabeStyle.equals("ausgeblendet")) {
                        AufgabeSearchFor = aufgabeXML;
                        AufgabeSearchFor.getChild("style").setText("erledigt");

                    }
                }
            }
            //Dokument als File speichern
            speichereXMLFile(document, rootElement, xmlFile);

        } catch (IOException | JDOMException io) {
            System.out.println(io.getMessage());
        }

        ausgeblendet = false;
        
        filterAlleAufgaben();
        filterAufgabe(aktiveListe);
    }

    /**
     * Macht eine erledigte Aufgabe wieder unerledigt (Style aendert sich)
     *
     * @param aufgabe Es wird die Aufgabe uebergeben in deren Zeile der Button
     * angeklickt wird aufgabe.style wird rueckgesetzt
     */
    public void resetErledigt(Aufgabe aufgabe) {

        SAXBuilder saxBuilder = new SAXBuilder(); //Baut einen XML-Leser auf Basis von SAX auf. Es wird nicht validiert.

        File xmlFile = getFile(); //File erzeugen

        try {

            Document document = (Document) saxBuilder.build(xmlFile); //Dokument erzeugen
            Element rootElement = document.getRootElement(); //Wurzelelement "aufgaben" einlesen

            //Suche nach einem bestimmten Knoten. Schleife über alle Aufgaben laufen lassen, bis die Aufgabe mit der id, die übergeben wurde, gefunden wird
            List ListAufgabe = rootElement.getChildren("aufgabe"); //Liste mit allen Kindern des Wurzelelements befüllen (hier alle Kinder "aufgabe" vom Wurzelelement "aufgaben"
            Iterator IteratorAufgabe = ListAufgabe.iterator();
            Element AufgabeSearchFor = null;

            while (IteratorAufgabe.hasNext()) {
                Element aufgabeXML = (Element) IteratorAufgabe.next();
                String aufgabeID = aufgabeXML.getAttributeValue("id");
                if (aufgabeID != null) {
                    if (aufgabeID.equals(String.valueOf(aufgabe.id))) {
                        AufgabeSearchFor = aufgabeXML;
                        AufgabeSearchFor.getChild("style").setText("null");
                        break;
                    }
                }
            }
            //Dokument als File speichern
            speichereXMLFile(document, rootElement, xmlFile);

        } catch (IOException | JDOMException io) {
            System.out.println(io.getMessage());
        }

        //aufgabe.style = null;
        filterAlleAufgaben();
        filterAufgabe(aktiveListe);
    }

    /**
     * Setzt alle Aufgaben zurueck (keine Aufgabe ist mehr erledigt)
     * aufgabe.style = null fuer alle Aufgaben
     */
    public void resetAlle() {

        SAXBuilder saxBuilder = new SAXBuilder(); //Baut einen XML-Leser auf Basis von SAX auf. Es wird nicht validiert.

        File xmlFile = getFile(); //File erzeugen

        try {

            Document document = (Document) saxBuilder.build(xmlFile); //Dokument erzeugen
            Element rootElement = document.getRootElement(); //Wurzelelement "aufgaben" einlesen

            //Suche nach einem bestimmten Knoten. Schleife über alle Aufgaben laufen lassen, bis die Aufgabe mit der id, die übergeben wurde, gefunden wird
            List ListAufgabe = rootElement.getChildren("aufgabe"); //Liste mit allen Kindern des Wurzelelements befüllen (hier alle Kinder "aufgabe" vom Wurzelelement "aufgaben"
            Iterator IteratorAufgabe = ListAufgabe.iterator();

            while (IteratorAufgabe.hasNext()) {
                Element aufgabeXML = (Element) IteratorAufgabe.next();
                aufgabeXML.getChild("style").setText("null");

            }

            //Dokument als File speichern
            speichereXMLFile(document, rootElement, xmlFile);

        } catch (IOException | JDOMException io) {
            System.out.println(io.getMessage());
        }

        /*for (Aufgabe aufgabe : aufgaben){
         aufgabe.setStyle(null);
         }*/
        filterAlleAufgaben();
        filterAufgabe(aktiveListe);
    }

    /**
     * Filtert die Aufgaben nach der ausgewaehlten Liste und nach der Erlaubnis für den eingeloggten User. Dafuer wird eine neue
     * "ArrayList" befuellt und anschließend angezeigt.
     *
     * @param listeGewaehlt Der Titel der Liste wird uebergeben
     * @return Die Liste wird angezeigt
     */
    public String filterAufgabe(String listeGewaehlt) {

        aufgabenGefiltert.clear();
        aufgaben = getAufgaben(); //aufgaben aus XML einlesen
        checkErlaubnis();

        for (Aufgabe aufgabe : aufgaben) {
            for (String liste : aufgabe.listen) {
                if ((String.valueOf(liste)).equals(String.valueOf(listeGewaehlt)) && (aufgabe.erlaubnis == true)) {
                    aufgabenGefiltert.add(aufgabe);
                }
            }
        }
        return "Liste";
    }

    /**
     * Filtert die Aufgaben nur nach Erlaubnis. Die gefilterten Aufgaben werden
     * unter "AlleAufgaben" angezeigt
     */
    public static void filterAlleAufgaben() {

        aufgabenAlleGefiltert.clear();
        aufgaben = getAufgaben(); //aufgaben aus XML einlesen
        checkErlaubnis();
        for (Aufgabe aufgabe : aufgaben) {
            if (aufgabe.isErlaubnis() == true) {
                aufgabenAlleGefiltert.add(aufgabe);
            }
        }
    }

    /**
     * Definition der Klasse "Aufgabe"
     * ----------------------------------------------------------------------
     */
    public static class Aufgabe {

        private boolean canEdit;        
        private String titel;
        private String prioritaet;
        private String beschreibung;
        private String faellig;
        private String style;
        private String[] listen; //Im Array "listen" werden die "ids" der Listen gespeichert, auf denen die Aufgabe sichtbar sein soll. Damit kann einer Aufgabe eindeutig eine Liste zugeordent werden, da die id einmalig ist.
        private boolean erlaubnis;
        private String erlaubteUser[];
        private int id;
        
        /**
         * Konstruktor fuer eine Aufgabe
         *
         * @param id id wird gesetzt
         * @param listen Listenzugehoerigkeit wird gesetzt
         * @param prioritaet Prioritaet wird gesetzt
         * @param titel Aufgabentitel wird gesetzt
         * @param beschreibung Aufgabenbeschreibung wird gesetzt
         * @param faellig Faelligkeitsdatum wird gesetzt
         * @param style style wird gesetzt
         * @param canEdit canEdit wird gesetzt
         * @param erlaubteUser erlaubteUser wird gesetzt
         */
        public Aufgabe(int id, String[] listen, String prioritaet, String titel, String beschreibung, String faellig, String style, boolean canEdit, String[] erlaubteUser) {
            this.id = id;
            this.listen = listen;
            this.prioritaet = prioritaet;
            this.titel = titel;
            this.beschreibung = beschreibung;
            this.faellig = faellig;
            this.style = style;
            this.canEdit = canEdit;
            this.erlaubteUser = erlaubteUser;
        }

        /**
         * Getter & Setter der Klasse Aufgabe
         */
        public boolean isCanEdit() {
            return canEdit;
        }

        public void setCanEdit(boolean canEdit) {
            this.canEdit = canEdit;
        }

        public String getPrioritaet() {
            return prioritaet;
        }

        public void setPrioritaet(String prioritaet) {
            this.prioritaet = prioritaet;
        }

        public String getTitel() {
            return titel;
        }

        public void setTitel(String titel) {
            this.titel = titel;
        }

        public String getBeschreibung() {
            return beschreibung;
        }

        public void setBeschreibung(String beschreibung) {
            this.beschreibung = beschreibung;
        }

        public String getFaellig() {
            return faellig;
        }

        public void setFaellig(String faellig) {
            this.faellig = faellig;
        }

        public String getStyle() {
            return style;
        }

        public void setStyle(String style) {
            this.style = style;
        }

        public String[] getErlaubteUser() {
            return erlaubteUser;
        }

        public void setErlaubteUser(String[] erlaubteUser) {
            this.erlaubteUser = erlaubteUser;
        }

        public boolean isErlaubnis() {
            return erlaubnis;
        }

        public void setErlaubnis(boolean erlaubnis) {
            this.erlaubnis = erlaubnis;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
        
        public String[] getListen() {
            return listen;
        }

        public void setListen(String[] listen) {
            this.listen = listen;
        }
    }

    /**
     * Ende der Definition der Klasse "Aufgabe"
     * ---------------------------------------------------------
     */
    
    
    /**
     * Getter & Setter der Klasse AufgabeData
     */
    public String getPrioritaet() {
        return prioritaet;
    }

    public void setPrioritaet(String prioritaet) {
        this.prioritaet = prioritaet;
    }

    public String getTitel() {
        return titel;
    }

    public void setTitel(String titel) {
        this.titel = titel;
    }

    public String getBeschreibung() {
        return beschreibung;
    }

    public void setBeschreibung(String beschreibung) {
        this.beschreibung = beschreibung;
    }

    public String getFaellig() {
        return faellig;
    }

    public void setFaellig(String faellig) {
        this.faellig = faellig;
    }

    public boolean isAusgeblendet() {
        return ausgeblendet;
    }

    public void setAusgeblendet(boolean ausgeblendet) {
        this.ausgeblendet = ausgeblendet;
    }

    public ArrayList<Aufgabe> getAufgabenGefiltert() {
        return aufgabenGefiltert;
    }

    public void setAufgabenGefiltert(ArrayList<Aufgabe> aufgabenGefiltert) {
        this.aufgabenGefiltert = aufgabenGefiltert;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getAktiveListe() {
        return aktiveListe;
    }

    public void setAktiveListe(String aktiveListe) {
        this.aktiveListe = aktiveListe;
    }

    public boolean isCanEdit() {
        return canEdit;
    }

    public void setCanEdit(boolean canEdit) {
        this.canEdit = canEdit;
    }

    public static String getAktiverUser() {
        return aktiverUser;
    }

    public static void setAktiverUser(String aktiverUser) {
        AufgabeData.aktiverUser = aktiverUser;
    }

    public static String[] getErlaubteUser() {
        return erlaubteUser;
    }

    public static void setErlaubteUser(String[] erlaubteUser) {
        AufgabeData.erlaubteUser = erlaubteUser;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setAufgaben(ArrayList<Aufgabe> aufgaben) {
        this.aufgaben = aufgaben;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }
    
    public ArrayList<Aufgabe> getAufgabenAlleGefiltert() {
        return aufgabenAlleGefiltert;
    }

    public void setAufgabenAlleGefiltert(ArrayList<Aufgabe> aufgabenAlleGefiltert) {
        AufgabeData.aufgabenAlleGefiltert = aufgabenAlleGefiltert;
    }
    
    public String[] getBearbeiteUser() {
        return bearbeiteUser;
    }

    public void setBearbeiteUser(String[] bearbeiteUser) {
        this.bearbeiteUser = bearbeiteUser;
    }
    
    public boolean isHinzufuegen() {
        return hinzufuegen;
    }

    public void setHinzufuegen(boolean hinzufuegen) {
        this.hinzufuegen = hinzufuegen;
    }

    public boolean isEntferne() {
        return entferne;
    }

    public void setEntferne(boolean entferne) {
        this.entferne = entferne;
    }
    
     public static List<String> getBearbeiteUserList() {
        return bearbeiteUserList;
    }

    public static void setBearbeiteUserList(List<String> bearbeiteUserList) {
        AufgabeData.bearbeiteUserList = bearbeiteUserList;
    }
    
    public String[] getListen() {
        return listen;
    }

    public void setListen(String[] listen) {
        this.listen = listen;
    }
}
