/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ldap;

/**
 * Übergabeklasse für die LDAP-Daten. Dabei werden ein paar Informationen aus
 * LDAP übergeben.
 *
 */
public class LdapDaten
{

  private String anzeigename = "";
  private String vorname = "";
  private String anrede = "";
  private String uid = "";
  private String mailadresse = "";
  private String username = "";
  private String nachname = "";
  private String funktion = "";

  /**
   *
   */
  public LdapDaten()
  {
  }

  @Override
  public String toString()
  {
    return anrede + " " + anzeigename + " (" + nachname + ", " + vorname + "), " + uid + "/" + username + ", " + mailadresse + ", " + funktion;
  }

  /**
   *
   * @return
   */
  public String getAnrede()
  {
    return anrede;
  }

  /**
   *
   * @param anrede
   */
  public void setAnrede(String anrede)
  {
    this.anrede = anrede;
  }

  /**
   *
   * @return
   */
  public String getAnzeigename()
  {
    return anzeigename;
  }

  /**
   *
   * @param anzeigename
   */
  public void setAnzeigename(String anzeigename)
  {
    this.anzeigename = anzeigename;
  }

  /**
   *
   * @return
   */
  public String getMailadresse()
  {
    return mailadresse;
  }

  /**
   *
   * @param mailadresse
   */
  public void setMailadresse(String mailadresse)
  {
    this.mailadresse = mailadresse;
  }

  /**
   *
   * @return
   */
  public String getNachname()
  {
    return nachname;
  }

  /**
   *
   * @param nachname
   */
  public void setNachname(String nachname)
  {
    this.nachname = nachname;
  }

  /**
   *
   * @return
   */
  public String getUid()
  {
    return uid;
  }

  /**
   *
   * @param uid
   */
  public void setUid(String uid)
  {
    this.uid = uid;
  }

  /**
   *
   * @return
   */
  public String getUsername()
  {
    return username;
  }

  /**
   *
   * @param username
   */
  public void setUsername(String username)
  {
    this.username = username;
  }

  /**
   *
   * @return
   */
  public String getVorname()
  {
    return vorname;
  }

  /**
   *
   * @param vorname
   */
  public void setVorname(String vorname)
  {
    this.vorname = vorname;
  }

  public String getFunktion()
  {
    return funktion;
  }

  public void setFunktion(String funktion)
  {
    this.funktion = funktion;
  }
  
  
}
