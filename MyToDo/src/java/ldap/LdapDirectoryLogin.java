/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ldap;

import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.AuthenticationException;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;

/**
 * Klasse zur Abfrage der Daten vom Ohm-ActiveDirectory. Im Volksmund wird
 * dieser Serverdienst auch "LDAP-Server" genannt.
 *
 * Das angegebene ActiveDirectory (im Volksmund auch "der LDAP-Server") werden
 * mit den gegebenen Usernamen/Passwort abgefragt. Dabei wird eine
 * SASL-gesichterte Verbindung (Digest-MD5) verwendet.
 *
 * Denkanstöße zu diesem Code stammen aus diesem Forum:
 * www.tutorials.de/java/222137-netzwerksache-aus-benutzername-werde-vor-und-nachname.html
 *
 * Beispiel für kompletten Eintrag: {displayname=displayname: Sepp Maier,
 * employeetype=employeetype: ST@EFI;ST@M-SY;, orcltimezone=orcltimezone:
 * Europe/Berlin, preferredlanguage=preferredlanguage: de-DE,
 * givenname=givenname: Sepp, orclactivestartdate=orclactivestartdate:
 * 20070927000000z, orclcalendarstore=orclcalendarstore: 00001,
 * objectclass=objectclass: top, person, inetorgperson, organizationalperson,
 * orcluser, orcluserv2, orcluserprovstatus, ctCalUser,
 * orcluserprovmode=orcluserprovmode: 20070928070328z, orclgender=orclgender:
 * Herr, departmentnumber=departmentnumber: Studiengang Master Elektronische und
 * Mechatronische Systeme, uid=uid: MaierSe35872, mail=mail:
 * seppma35872@th-nuernberg.de, cn=cn: seppma35872,
 * employeenumber=employeenumber: 20000035, o=o: Technische Hochschule
 * Nürnberg, sn=sn: Maier, orclisenabled=orclisenabled: ENABLED, title=title:
 * Student}
 *
 */
public class LdapDirectoryLogin
{

  // Standardpfad, von dem die Suche im ActiveDirectory ausgehen soll
  private final static String SEARCH_BASE = "cn=Users,dc=ohm-hochschule,dc=de";
  // ActiveDirectory (Volksmund: "LDAP-Server")
  private final static String LDAP_URL = "ldap://sso.cs.ohm-hochschule.de:389";
  private final static boolean DEBUG = true;

  /**
   * Diese Funktion testet, ob die übergebenen Zugangsdaten (E-Mail-Adresse,
   * Passwort) korrekt sind und gibt bei Erfolg einige Userdaten zurück. Im
   * Fehlerfall wird null zurückgegeben. Die Funktion sucht im LDAP nach der
   * gegebenen Mail-Adresse und gibt den zugehörigen Benutzer zurück. Da die
   * Adresse eindeutig sein sollte, wird nur das erste Ergebnis berücksichtigt.
   *
   * @param username
   * @param password
   * @return LdapDaten oder null bei Fehler.
   */
  public LdapDaten findUser(String username, String password)
  {
    // Variable für Rückgabe.
    LdapDaten daten = null;

    Properties env = new Properties();
    env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
    //env.put(Context.SECURITY_AUTHENTICATION, "DIGEST-MD5");
    env.put(Context.SECURITY_AUTHENTICATION, "simple");
    //env.put(Context.SECURITY_AUTHENTICATION, "none");
    env.put(Context.SECURITY_PRINCIPAL, "cn=" + username + ",cn=Users,dc=ohm-hochschule,dc=de");
    // bei leerem Passwort wird automatisch anonym zugegriffen -> unterbinden!
    String pw = password.isEmpty() ? "müllasdf4556gdfgcbqäöü" : password; 
    //String pw = "";
    env.put(Context.SECURITY_CREDENTIALS, pw);
    env.put(Context.PROVIDER_URL, LDAP_URL);

    /*
     * Beim Erzeugen des LDAP-Kontextes wird die Verbindung mit den
     * gewünschten Anmeldedaten aufgebaut.
     */
    DirContext ctx;
    try
    {
      ctx = new InitialLdapContext(env, null);
    }
    catch (AuthenticationException ex)
    {
      if (DEBUG)
      {
        Logger.getLogger(LdapDirectoryLogin.class.getName()).log(Level.SEVERE, "ungültige Zugangsdaten, Authentifizierung fehlgeschlagen.", ex);
      }
      return null;
    }
    catch (NamingException ex)
    {
      if (DEBUG)
      {
        Logger.getLogger(LdapDirectoryLogin.class.getName()).log(Level.SEVERE, null, ex);
      }
      return null;
    }

    /*
     * Testweises Ausgeben der möglichen SASL-Methoden mit folgenden beiden
     * Zeilen möglich. Zum Zeitpunkt der Programmierung wurde nur Digest-MD5
     * angeboten, aber nicht unterstützt. :(
     */
        // Attributes attrs = ctx.getAttributes(PROVIDER_URL, new String[]{"supportedSASLMechanisms"});
    // System.out.println(attrs.get("supportedSASLMechanisms"));

    /*
     * Die LDAP-Abfrage wird zusammengesetzt...
     */
    String searchFilter = "(&(cn=" + username + "))";
        //System.out.println("Verwendeter Suchauftrag im LDAP: " + searchFilter);

    /*
     * Die Eigenschaften angegeben, welche abgefragt werden sollen.
     */
    SearchControls searchControls = new SearchControls();
    String[] resultAttributes =
    {
      "displayname", "givenname", "orclgender", "uid", "mail", "cn", "sn", "employeetype"
    };
    searchControls.setReturningAttributes(resultAttributes);
    searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);

    /*
     * Eigentliche Suche im ActiveDirectory durchführen
     */
    NamingEnumeration results;
    try
    {
      results = ctx.search(SEARCH_BASE, searchFilter, searchControls);
    }
    catch (NamingException ex)
    {
      if (DEBUG)
      {
        Logger.getLogger(LdapDirectoryLogin.class.getName()).log(Level.SEVERE, null, ex);
      }
      return null;
    }

    /*
     * E-Mail-Adressen sollten (selbst im LDAP) eindeutig sein. Daher wird
     * nur das erste Element berücksichtigt.
     */
    if (results.hasMoreElements())
    {
      SearchResult sr = (SearchResult) results.nextElement();
      Attributes attrib = sr.getAttributes();
      daten = new LdapDaten();
      try
      {
        daten.setAnzeigename(attrib.get("displayname").get(0).toString());
        daten.setVorname(attrib.get("givenname").get(0).toString());
        daten.setAnrede(attrib.get("orclgender").get(0).toString());
        daten.setUid(attrib.get("uid").get(0).toString());
        daten.setMailadresse(attrib.get("mail").get(0).toString());
        daten.setUsername(attrib.get("cn").get(0).toString());
        daten.setNachname(attrib.get("sn").get(0).toString());
        daten.setFunktion(attrib.get("employeetype").get(0).toString());
      }
      catch (NamingException ex)
      {
        /*
         * Sollte nicht passieren, der Programmierer sollte in den
         * Attributen oben und hier das gleiche angeben, der Server
         * sollte das gleiche liefern...
         */
        if (DEBUG)
        {
          Logger.getLogger(LdapDirectoryLogin.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
      }
    }

    if (ctx != null)
    {
      try
      {
        ctx.close();
      }
      catch (NamingException ex)
      {
        if (DEBUG)
        {
          Logger.getLogger(LdapDirectoryLogin.class.getName()).log(Level.SEVERE, null, ex);
        }
      }
    }

    return daten;
  }
}
