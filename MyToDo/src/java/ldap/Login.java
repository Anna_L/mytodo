package ldap;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;

/*
* Klasse zur Verwaltung der Benutzer
*/

@SessionScoped

@Named("Login")
public class Login implements Serializable{
    
    private String name;  // Username
    private String pw; //Passwort
    private String anzeige; //Login-Informationen
    private String suchanzeige; //Suchergebnis-Anzeige
    private boolean eingeloggt = false; //wird true, wenn ein Benutzer eingeloggt ist
    private String erlaubterUser;
    private String[] erlaubteUser;
    private String[] erlaubteUserBearb;
    private boolean userhinzugefuegt; //wird true, wenn mindestens ein User hinzugefügt wurde
    
    private ArrayList<String> erlaubteUserList = new ArrayList<>();
    private List<String> erlaubteUserListBearb = new ArrayList<>();

    private LdapDirectoryLogin ldap = new LdapDirectoryLogin();
    private LdapDaten daten;
    private LdapDirectorySearch search = new LdapDirectorySearch();
    private LdapDaten suchergebnis;
    
    /**
     * Prueft die eingegebenen Daten auf Richtigkeit
     * Ruft die Funktionen auf, die nötig sind um
     * die Berechtigung für Aufgaben und Listen zu setzen
     * 
     * @return Weiterleitung zu "AlleAufgaben" bzw. Kein-Zugriff-Meldung
     */
    public String login(){
    daten = ldap.findUser(name, pw);
    if (daten != null)
    {
        
        anzeige = "Eingeloggt als " + daten.getAnzeigename();
        eingeloggt = true;
        mytodo.ListeData.setAktiverUser(daten.getUsername());
        mytodo.ListeData.checkErlaubnis();
        mytodo.ListeData.filterListen();
        mytodo.AufgabeData.setAktiverUser(daten.getUsername());
        mytodo.AufgabeData.checkErlaubnis();
        mytodo.AufgabeData.filterAlleAufgaben();
        
        return "AlleAufgaben";
    }
    else
    {
        anzeige = ("Kein Zugriff");
        return "KeinZugriff";
    }
    }
    
    /**
     * User wird ausgeloggt, name und pw werden auf null gesetzt,
     * damit inputText leer erscheint
     * 
     * @return gibt Ausgeloggt-Meldung zurueck
     */
    public String logout(){
        mytodo.AufgabeData.resetErlaubnis();
        mytodo.ListeData.resetErlaubnis();
        mytodo.AufgabeData.setAktiverUser(null);
        mytodo.ListeData.setAktiverUser(null);
        eingeloggt = false;
        name = null;
        pw = null;
        return "Ausgeloggt";
    }
    
    /**
     * Prueft, ob es den eingegebenen Usernamen gibt
     * Setzt die erlaubten User von Liste und Aufgabe wenn ein User hinzugefügt wird
     * Zeigt Rückmeldung an, ob Benutzer gefunden wurde oder nicht
     * 
     * @param name Es wird nach dem eingegebenen Usernamen gesucht
     */
    public void suche(String name)
    {
        String passwort = ""; //Zur Suche wird ein leeres Passwort übergeben
        
        suchergebnis = search.findUser(name, passwort);
        if (suchergebnis != null)
        {
            erlaubteUserList.add(erlaubterUser);
            erlaubteUser = erlaubteUserList.toArray(new String[erlaubteUserList.size()]);
            suchanzeige = erlaubterUser + " wurde hinzugefügt";
            mytodo.AufgabeData.setErlaubteUser(erlaubteUser);
            mytodo.ListeData.setErlaubteUser(erlaubteUser);
            userhinzugefuegt = true;
        }
        else
        {
            suchanzeige = "Benutzer nicht gefunden";
        }
    }
    
   
    
    /**
     * Prueft, ob es den eingegebenen Usernamen gibt,
     * wenn eine Aufgabe bearbeitet wird
     * Fügt den Benutzer hinzu, falls er gefunden wurde
     * Zeigt an, ob der Benutzername gefunden wurde oder nicht
     * 
     * @param name Es wird nach dem eingegebenen Usernamen gesucht
     */
    public void UserHinzufuegen(String name)
    {
        String passwort = ""; //Zur Suche wird ein leeres Passwort übergeben
        
        suchergebnis = search.findUser(name, passwort);
        erlaubteUserListBearb = mytodo.AufgabeData.getBearbeiteUserList();
        if (suchergebnis != null)
        {
            erlaubteUserListBearb.add(erlaubterUser);
            erlaubteUserBearb = erlaubteUserListBearb.toArray(new String[erlaubteUserListBearb.size()]);
            suchanzeige = erlaubterUser + " wurde hinzugefügt";
            mytodo.AufgabeData.setErlaubteUser(erlaubteUserBearb);
            userhinzugefuegt = true;
        }
        else
        {
            suchanzeige = "Benutzer nicht gefunden";
        }
    }
    
    /**
     * Prueft, ob es den eingegebenen Usernamen gibt,
     * wenn eine Liste bearbeitet wird
     * Fügt den Benutzer hinzu, falls er gefunden wurde
     * Zeigt an, ob der Benutzername gefunden wurde oder nicht
     * 
     * @param name Es wird nach dem eingegebenen Usernamen gesucht
     */
    public void UserZuListeHinzufuegen(String name)
    {
        String passwort = ""; //Zur Suche wird ein leeres Passwort übergeben
        
        suchergebnis = search.findUser(name, passwort);
        erlaubteUserListBearb = mytodo.ListeData.getBearbeiteUserList();
        if (suchergebnis != null)
        {
            erlaubteUserListBearb.add(erlaubterUser);
            erlaubteUserBearb = erlaubteUserListBearb.toArray(new String[erlaubteUserListBearb.size()]);
            suchanzeige = erlaubterUser + " wurde hinzugefügt";
            mytodo.ListeData.setErlaubteUser(erlaubteUserBearb);
            userhinzugefuegt = true;
        }
        else
        {
            suchanzeige = "Benutzer nicht gefunden";
        }
    }
    
    /**
     * Leeren der ArrayList erlaubteUserList,
     * damit die erlaubteUserList im nächsten
     * Bearbeitungsvorgang neu gefüllt werden kann
     */
    public void clearUserList(){
        erlaubteUserList.clear();
    }
    
    /**
     * Getter & Setter fuer die Klasse Login
     */
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPw() {
        return pw;
    }

    public void setPw(String pw) {
        this.pw = pw;
    }

    public LdapDirectoryLogin getLdap() {
        return ldap;
    }

    public void setLdap(LdapDirectoryLogin ldap) {
        this.ldap = ldap;
    }

    public LdapDaten getDaten() {
        return daten;
    }

    public void setDaten(LdapDaten daten) {
        this.daten = daten;
    }
    
    public String getAnzeige() {
        return anzeige;
    }

    public void setAnzeige(String anzeige) {
        this.anzeige = anzeige;
    }
    
    public boolean isEingeloggt() {
        return eingeloggt;
    }

    public void setEingeloggt(boolean eingeloggt) {
        this.eingeloggt = eingeloggt;
    }
    
    public String[] getErlaubteUser() {
        return erlaubteUser;
    }

    public void setErlaubteUser(String[] erlaubteUser) {
        this.erlaubteUser = erlaubteUser;
    }
    
    public String getSuchanzeige() {
        return suchanzeige;
    }

    public void setSuchanzeige(String suchanzeige) {
        this.suchanzeige = suchanzeige;
    }
    
    public boolean isUserhinzugefuegt() {
        return userhinzugefuegt;
    }

    public void setUserhinzugefuegt(boolean userhinzugefuegt) {
        this.userhinzugefuegt = userhinzugefuegt;
    }
    
    public String getErlaubterUser() {
        return erlaubterUser;
    }

    public void setErlaubterUser(String erlaubterUser) {
        this.erlaubterUser = erlaubterUser;
    }
    
    public LdapDirectorySearch getSearch() {
        return search;
    }

    public void setSearch(LdapDirectorySearch search) {
        this.search = search;
    }

    public LdapDaten getSuchergebnis() {
        return suchergebnis;
    }

    public void setSuchergebnis(LdapDaten suchergebnis) {
        this.suchergebnis = suchergebnis;
    }
    
    public ArrayList<String> getErlaubteUserList() {
        return erlaubteUserList;
    }

    public void setErlaubteUserList(ArrayList<String> erlaubteUserList) {
        this.erlaubteUserList = erlaubteUserList;
    }

    public List<String> getErlaubteUserListBearb() {
        return erlaubteUserListBearb;
    }

    public void setErlaubteUserListBearb(List<String> erlaubteUserListBearb) {
        this.erlaubteUserListBearb = erlaubteUserListBearb;
    }
    
    public String[] getErlaubteUserBearb() {
        return erlaubteUserBearb;
    }

    public void setErlaubteUserBearb(String[] erlaubteUserBearb) {
        this.erlaubteUserBearb = erlaubteUserBearb;
    }
}