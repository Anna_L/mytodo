/*
MyToDo
Authors: Svenja Lachmann, Anna Linke, Alisa Reiter
*/
/*
 * Diese Funktion zeigt einen Kalender zur Datumswahl (Datepicker) an, der von JQueryUi zur Verfügung gestellt wird
 */

$(document).ready(function() {
    $( ".datepicker" ).datepicker();
});