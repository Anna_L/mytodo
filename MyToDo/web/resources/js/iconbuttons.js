/*
MyToDo
Authors: Svenja Lachmann, Anna Linke, Alisa Reiter
*/
/*
 * Diese Funktionen legen die Darstellung der Icon-Buttons (JQueryUI) fest
 */

$(document).ready(function() {
     $( ".iconErledigt" ).button({
        icons: {
            primary: "ui-icon-check"
        },
        text: false
    });
    $(".iconNichtErledigt").button({
        icons: {
            primary: "ui-icon-arrowreturnthick-1-w"
        },
        text: false
    });
    $(".iconBearbeiten").button({
        icons: {
            primary: "ui-icon-pencil"
        },
        text: false
    });
    $(".iconAlleBearbeiten").button({
        icons: {
            primary: "ui-icon-folder-open"
        },
        text: false
    });
    $(".iconSpeichern").button({
        icons: {
            primary: "ui-icon-disk"
        },
        text: false
    });
    $(".iconLoeschen").button({
        icons: {
            primary: "ui-icon-trash"
        },
        text: false
    });
    $(".iconEntfernen").button({
        icons: {
            primary: "ui-icon-closethick"
        },
        text: false
    });
});
