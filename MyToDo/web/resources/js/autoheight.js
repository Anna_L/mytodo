/*
MyToDo
Authors: Svenja Lachmann, Anna Linke, Alisa Reiter
*/
/*
 * Diese Funktion passt die Höhe des Menüs und des Anzeigebereichs automatisch aneinander an
 */

$(document).ready(function() {
    var max_height = 0;
    $('.autoheight').each(function() {
        h = $(this).height();
        if(typeof(h) !== "undefined") {
            if(h > max_height) {
            max_height = h;
            }
        }
    });
    
    if(max_height > 0) {
        $('.autoheight').height(max_height);
    }
});