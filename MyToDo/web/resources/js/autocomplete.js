/*
MyToDo
Authors: Svenja Lachmann, Anna Linke, Alisa Reiter
*/

/*
 * Diese Funktion dient zur automatischen Vervollstädigung der Benutzereingabe
 * In Zukunft könnte diese Funktion so erweitert werden, dass Sie für alle Benutzer zur Verfügung steht
 */

$(document).ready(function() {
    $( ".autocomplete" ).autocomplete({
        source: [ "lachmannsv50175", "linkean50148", "reiteral50350"]
    });
});